<div class="container-fluid intro" id="about">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="chain">Сервис</h1>
            <p class="text-intro">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 section-1 nopadding" id="work">
            <div class="logo-1 wp1"></div>
        </div>
        <div class="col-md-8 section-text nopadding">
            <div class="wp4">
                <h2 class="frame">Прокат</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.</p>
                <div class="thin-sep"></div>
            </div>
            <div class="small-featured-img seat-red">
                <div class="arrow"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 section-text nopadding">
            <div class="wp5">
                <h2 class="mech">La Boriosa</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.</p>
                <div class="thin-sep"></div>
            </div>
            <div class="small-featured-img seat-black">
                <div class="arrow"></div>
            </div>
        </div>
        <div class="col-md-8 section-2 nopadding">
            <div class="logo-2 wp2"></div>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-4 section-3">
            <div class="logo-3 wp3"></div>
        </div>
        <div class="col-md-4 section-text nopadding">
            <div class="wp6">
                <h2 class="front-frame">Retrò Bike - M. Hulot</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived.</p>
                <div class="thin-sep"></div>
            </div>
            <div class="small-featured-img frame-red">
                <div class="arrow"></div>
            </div>
        </div>
        <div class="col-md-4 section-4"></div>
    </div>
</div>
<section class="flex-container">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="flex-twitter-icon"></div>
                            <h2 class="twitter-post-username">
                                <a href="#">AOS New York @aod</a> <span>/ 35 min</span>
                            </h2>
                            <p class="twitter-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus. Nullam a odio porttitor, dictum turpis vitae, pretium ante amet.</p>
                        </li>
                        <li>
                            <div class="flex-twitter-icon"></div>
                            <h2 class="twitter-post-username">
                                <a href="#">AOS New York @aod</a> <span>/ 35 min</span>
                            </h2>
                            <p class="twitter-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus. </p>
                        </li>
                        <li>
                            <div class="flex-twitter-icon"></div>
                            <h2 class="twitter-post-username">
                                <a href="#">AOS New York @aod</a> <span>/ 35 min</span>
                            </h2>
                            <p class="twitter-post">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed ut tellus ac nulla semper rhoncus. Nullam a odio porttitor.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container-fluid" id="shop">
    <div class="row">
        <div id="effect" class="effects clearfix">
            <div class="col-md-4 left nopadding">
                <div class="left-box-1">
                    <div class="img">
                        <img src="img/l1.jpg" alt="Leather Seats">
                        <div class="overlay">
                            <a href="http://google.com/" class="expand">leather seats</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="left-box-2 box">
                    <div class="img">
                        <img src="img/l2.jpg" alt="Custom Seats">
                        <div class="overlay">
                            <a href="#" class="expand">custom seats</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="left-box-btm box">
                    <div class="img">
                        <img src="img/l3.jpg" alt="Limited Edition">
                        <div class="overlay">
                            <a href="#" class="expand">limited edition</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mid nopadding">
                <div class="mid-box-1 box">
                    <div class="img">
                        <img src="img/l4.jpg" alt="Shop Bags">
                        <div class="overlay">
                            <a href="#" class="expand">shop bags</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="mid-box-2 box">
                    <div class="img">
                        <img src="img/l5.jpg" alt="Shop Bikes">
                        <div class="overlay">
                            <a href="#" class="expand">shop bikes</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 right nopadding">
                <div class="right-box-1 box">
                    <div class="img">
                        <img src="img/l6.jpg" alt="Shop Now">
                        <div class="overlay">
                            <a href="#" class="expand">shop now</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="right-box-2 box">
                    <div class="img">
                        <img src="img/l7.jpg" alt="Shop Seats">
                        <div class="overlay">
                            <a href="#" class="expand">shop seats</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
                <div class="right-box-3 box">
                    <div class="img">
                        <img src="img/l8.jpg" alt="Shop Accessories">
                        <div class="overlay">
                            <a href="#" class="expand">shop accessories</a>
                            <a class="close-overlay hidden">x</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
