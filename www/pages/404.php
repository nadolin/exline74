<div class="container-fluid intro" id="about">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h1 class="chain">404 - страница не найдена.</h1>
        </div>
    </div>
</div>

<div class="container-fluid" id="shop">
    <div class="row">
        <div id="effect" class="effects clearfix">
            <div class="col-md-4 left nopadding">
                <div class="left-box-1">
                    <div class="img">
                        <img src="img/l1.jpg" alt="Leather Seats">
                    </div>
                </div>
                <div class="left-box-2 box">
                    <div class="img">
                        <img src="img/l2.jpg" alt="Custom Seats">

                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="left-box-btm box">
                    <div class="img">
                        <img src="img/l3.jpg" alt="Limited Edition">
                    </div>
                </div>
            </div>
            <div class="col-md-4 mid nopadding">
                <div class="mid-box-1 box">
                    <div class="img">
                        <img src="img/l4.jpg" alt="Shop Bags">
        
                    </div>
                </div>
                <div class="mid-box-2 box">
                    <div class="img">
                        <img src="img/l5.jpg" alt="Shop Bikes">

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 right nopadding">
                <div class="right-box-1 box">
                    <div class="img">
                        <img src="img/l6.jpg" alt="Shop Now">
   
                    </div>
                </div>
                <div class="right-box-2 box">
                    <div class="img">
                        <img src="img/l7.jpg" alt="Shop Seats">
                     </div>
                </div>
                <div class="right-box-3 box">
                    <div class="img">
                        <img src="img/l8.jpg" alt="Shop Accessories">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>