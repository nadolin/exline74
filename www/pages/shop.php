<div class="container-fluid intro">
<h1 class="chain">Магазин запчастей</h1> 
<div class="col-md-6 col-md-offset-3">
   <div class="row" align="center"> <h2>Для сноубордов и горных лыж</h2>
<table class="x-table table table-hover table-striped x-table--big" align="left"> 
	<tbody> 
		<tr> 
			<th>Наименование</th>
			<th>Стоимость, руб</th>
 		</tr>
		<tr> 
			<td>Бамперы для сноуборда (комплект, цвета в ассортрименте)</td>
			<td>500</td>
 		</tr>
		<tr> 
			<td>Бамперы для горных лыж (комплект)</td>
			<td>300</td>
 		</tr>
		<tr> 
			<td>Ремешки в ассортименте</td>
			<td>250</td>
 		</tr>
		<tr> 
			<td>Стреп верхний</td>
			<td>700</td>
 		</tr>
		<tr> 
			<td>Стреп нижний</td>
			<td>550</td>
 		</tr>
		<tr> 
			<td>Диск крепления (пара)</td>
			<td>250</td>
 		</tr>
		<tr> 
			<td>Болты для креплений (комплект)</td>
			<td>400</td>
 		</tr>
		<tr> 
			<td>Парафин ПЛИМ универсальный (-2<sup>о</sup>С −20<sup>о</sup>С, на любой тип снега, 200гр)</td>
			<td>500</td>
 		</tr>
 	</tbody>
 </table> 

  </div>
 
<div class="row" align="center">
 <h2 >Для велосипедов</h2> 
			<div>Больше не нужно бегать по магазинам по всему городу! <br>У нас Вы можете приобрести велосипедные запчасти и аксессуары и, при необходимости, установить их.<br>Большой ассортимент и доступные цены приятно Вас удивят! </div>
		
			<img src="img/bicycle-1.png" alt="Bicycle" align="center" height="50%" width="50%">
   </div>
   

</div></div>
