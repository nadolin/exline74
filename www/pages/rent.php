 <h1 class="frame">Зимний прокат</h1>
			<h2 align="center">Сезон 2019-2020</h2>
			
			<div> 
	<table class="x-table x-table--big table table-hover table-striped" align="center" > 
		<tbody>
			<tr> 
				<td> </td>
				<td> 
					 Пн - Пт 
				</td>
				<td> 
					 Сб – Вс 
				</td>
			</tr>
			<tr> 
				<td> 
					 Комплект сноуборд / горные лыжи 
				</td>
				<td class="a_c"> 
					 350 руб 
				</td>
				<td class="a_c"> 
					 500 руб 
				</td>
			</tr>
			<tr> 
				<td> 
					 Отдельно сноуборд / горные лыжи 
				</td>
				<td class="a_c"> 
					 250 руб 
				</td>
				<td class="a_c"> 
					 400 руб 
				</td>
			</tr>
			<tr> 
				<td> 
					 Отдельно ботинки 
				</td>
				<td class="a_c"> 
					 150 руб 
				</td>
				<td class="a_c"> 
					 300 руб 
				</td>
			</tr>
			<tr> 
				<td> 
					 Маска/Шлем/Защита 
				</td>
				<td class="a_c"> 
					 100 руб 
				</td>
				<td class="a_c"> 
					 150 руб 
				</td>
			</tr>
			<tr>
			
			
			
		<td colspan="4" align="left"><strong> <div align="center"> Цена указана за сутки</div><br/>
		<br><strong>Варианты залога за ОДИН комплект:<br/>
 •	1000 рублей + Документ (паспорт РФ, загран.паспорт или в\у)<br/>
 •	2 Документа<br/>
 •	Денежный залог от 10000 рублей</strong><br/>
	<br/>
 ВНИМАНИЕ! СНИЛС, Военный билет, Студенческий билет и иные документы для залога НЕ подходят!<br/></td>
	<br/>
 </td>
		</tr>
		</tbody>
	</table>
</div>

<div class="container-fluid intro"> 

 

 <!-- <div class="row">
        <div class="col-md-6 col-md-offset-3">  -->
            <h1 class="frame">Летний прокат</h1>
			<h2 align="center">Сезон 2019</h2>
				

            <table class="x-table x-table--big table table-hover table-striped" align="center" >
				
				<tbody>
			<tr> 
			<td></td>
			<td>Детский <br>(до 24' дюймов)</td>
			<td>Взорслый</td>
			<td>Велосипед + детское кресло / Фэтбайк</td>
		</tr>
		<tr> 
			<td>Первый час</td>
			<td>150 руб</td>
			<td>200 руб</td>
			<td>250 руб</td>
		</tr>
		<tr> 
			<td>Второй час</td>
			<td>100 руб</td>
			<td>100 руб</td>
			<td>150 руб</td>
		</tr>
		<tr> 
			<td>Третий и последующие</td>
			<td>50 руб</td>
			<td>50 руб</td>
			<td>100 руб</td>
		</tr>
		<tr> 
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr> 
			<td>Ночной прокат</td>
			<td>300 руб</td>
			<td>300 руб</td>
			<td>400 руб</td>
		</tr>
		<tr> 
			<td>1 сутки</td>
			<td>700 руб</td>
			<td>700 руб</td>
			<td>1000 руб</td>
		</tr>
		<tr> 
		<td colspan="4" align="left"> <br><strong>Варианты залога за ОДИН велосипед:<br/>
 - 1000 рублей + Документ (паспорт РФ, загран.паспорт или в\у)<br/>
 - Денежный залог 15000 рублей</strong><br/>
	<br/>
 ВНИМАНИЕ! СНИЛС, Военный билет, Студенческий билет и иные документы для залога НЕ подходят!<br/>
	<br/>
 На прокат также можно взять детские сиденья и велошлем. Велокресло и шлем выдаются только с велосипедом.<br/>
 Велосипеды импортные, многоскоростные. Среди марок - Specialized, Wheeler, Jamis, GT. Большой выбор цветов и ростовок.<br/></td>
		</tr>
				</tbody>
				</table>	

	
</div>
 		
		 	