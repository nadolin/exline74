<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$pageTitle?></title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/styles.css?v=1.4" rel="stylesheet">
    <link href="css/queries.css?v=1.4" rel="stylesheet">
    <link href="css/custom.css?v=1.4" rel="stylesheet">
    <link rel="stylesheet" href="css/flexslider.css?v=1.2" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<header>
    <div class="container-fluid">
        <div class="row">
            <div class="pullcontainer">
                <a href="#" id="pull"><i class="fa fa-bars fa-2x"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 col-md-offset-2 col-logo">
                <a href='/'><img class="main-logo" src="img/main-logo.png"></a>
            </div>
            <div class="col-md-4 col-contacts">
                <p><a class="phone-link" href="tel:+79000245180">8-900-024-51-80</a><br>
                    <a href="/?p=contacts" class="address-link">Челябинск<br>
                        Молодогвардейцев, 35</a>
                </p>
            </div>
        </div>
        <div class="row row-nav">
            <div class="col-md-8 col-md-offset-2">
                <nav>
                    <ul class="clearfix">
                        <li><a href="?p=service" >Сервис</a></li>
                        <li><a href="?p=rent">Прокат</a></li>
						 <li><a href="?p=shop">Магазин</a></li>
                        <li><a href="?p=contacts">Как проехать</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!--<div class="hero"></div>-->
</header>
