<footer id="contact">
    <div class="container">
        <div class="row">
            <div class="col-md-4 footer-leftcol">
                <p><span class="bold-16p">Челябинск</span><br>
                    Молодогвардейцев, 35<br>
					<a href="mailto:extremeline74@mail.ru">Написать на e-mail</a>
                </p>
            </div>
            <div class="col-md-4 footer-midcol">
                <ul>
                    <li><a href="https://vk.com/extremeline74" target='_blank' class="vk-icon"></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-4 footer-rightcol">

            </div>
        </div>
    </div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins)-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.flexslider.js"></script>
<script src="js/scripts.js"></script>
<script src="js/modernizr.js"></script>
<script src="js/waypoints.min.js"></script>

<!-- version 1.0 -->
</body>
</html>
