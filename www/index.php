<?php
global $CONFIG;
global $pageTitle;

require_once('./config.php');



$pages = ['index', '404', 'service', 'rent', 'shop', 'contacts'];

$requestedPage = $_GET['p'];
$pagePath = "./pages/{$requestedPage}.php";

if ($requestedPage == '') {	
   $requestedPage = 'index';
   $pagePath = "./pages/{$requestedPage}.php";
} else if (file_exists($pagePath) && in_array($requestedPage, $pages)) {
	// пииисикааак, молчит и ничего не делает
} else {
   $requestedPage = '404';
}

$pagePath = "./pages/{$requestedPage}.php";

$pageTitle = $CONFIG['pageTitles'][$requestedPage];

require_once("./header.php");
require_once($pagePath);
require_once("./footer.php");
